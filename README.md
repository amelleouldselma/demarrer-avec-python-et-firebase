# Création d'une todoList 

Vous pouvez retrouver ma todoList ou vous pouvez ajouter des taches à faire. Les techno utiliser ont été **Python**, **Django** et **Firebase**.



# Installation du projet

° Cloner le repo

    git clone https://gitlab.com/amelleouldselma/demarrer-avec-python-et-firebase.git
    cd demarrer-avec-python-et-firebase

° Installer Django && Pyrebase

    pip install -r requirements.txt

    python3 manage.py migrate
    python3 manage.py createsuperuser

° Sur *Firebase*

    Crée un compte puis crée une application web

    Crée un utilisateur et autorosier l'autentification avec un email et mpd

    Crée une realtime Database (Don't forget les droits dans "régles" )



# Lancer l'application

° python3 manage.py runserver
° Indentifier vous maintenant avec l'email et le mpd utiliser lors de la création de votre premier user dans firebase
° Vous pouvez ajouter une tache maintenant ^^